<?php
header("Content-Type: text/html; charset=utf-8");
require "config.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
  <title>
    Главная страница
  </title>
  <link rel="stylesheet" href="style/bootstrap.css">
  <link rel="stylesheet" href="style/style.css">
  <script type="text/javascript" src="script/jquery-1.8.2.min.js"></script>
</head>


<body>

<?php
if ($auth->AuthCheck()):

    if (isset($_GET['exit'])) {
        $auth->Logout();
    }
    if (isset($_POST['addEvent'])) {
        $result = $auth->AddEvent('picture', 'title', 'description', 'date', 'login', 'category');
        echo $result;
    };
    ?>

    <div class="containerS">
        <div class="eventsbox">
            <?php
			if (isset($_GET['my']))
			{
				Auth::MyOutput($_SESSION['login']);
			}
			else if(isset($_GET['sub']))
			{
				Auth::SubOutput($_SESSION['login']);
			}
			else{
            Auth::GeneralOutput();
			}
            echo '</div>';
            ?>

            <script>
                $(document).ready(function () {
                    $(".subscribe").click(function () {
                        $.ajax(
                            {
                                type: "POST",
                                url: "subscribe.php",
                                data: {id: this.id},
                                success: function (data) {

                                    var notif = document.getElementById("notif");
                                    if (data === "Вы подписались на событие.") {
                                        notif.style.display = 'block';
                                        notif.className = "alert alert-success";
                                        notif.innerHTML = "Вы успешно подписались на событие";
                                    }
                                    else {
                                        notif.style.display = 'block';
                                        notif.className = "alert alert-info";
                                        notif.innerHTML = data;
                                    }
                                    setTimeout(function () {
                                        notif.style.display = 'none';
                                    }, 3000);
                                },
                                error: function () {

                                }
                            }
                        );

                    });
                });
            </script>

            <div class="personsbox">
                <div class="welcome">
                    <a href="#box1" class="buttonS" style=width:100%;><?php echo $_SESSION['login']; ?></a>
                </div>

                <div class="welcome">
                    <?php
						$qstring = 'SELECT * FROM `event_category`,`category` WHERE `event_category`.idcategory=`category`.id and login="' . $_SESSION['login'] . '";';
						$result = mysqli_query(Auth::$connection, $qstring) or die("Ошибочный запрос к БД" . mysqli_error());
						$row = mysqli_fetch_array($result);
						do {
							echo('<a class="category">'.$row['name'].'</a><br/>');
						} while ($row = mysqli_fetch_array($result));
					?>
                </div>

                <a href="#box" class="buttonS" style=width:100%;>Добавить запись</a>
                <a class="buttonS" href="mainpage.php?my=true" style=width:100%;>Мои события</a>
				<a class="buttonS" href="mainpage.php?sub=true" style=width:100%;>Избранные события</a>
				<a class="buttonS" href="mainpage.php" style=width:100%;>Все события</a>
                <a class="buttonS" href="mainpage.php?exit=true" style=width:100%;>Выйти</a>

            </div>
            <a href="#x" class="overlay" id="box"></a>

            <div class="popup">

                <p>Добавьте свое событие</p>
                <form enctype="multipart/form-data" action="mainpage.php" method="POST">
                    <div class="form-group">
                        <div class="rloc">Название</div>
                        <input type=text name="title"/>
                    </div>

                    <div class="form-group">
                        <div class="rloc">Описание</div>
                        <textarea name="description" rows="5"></textarea>
                    </div>

                    <input type="hidden" name="MAX_FILE_SIZE" value="819200"/>
                    <div class="form-group">
                        <div class="rloc">Выбрать картинку</div>
                        <input name="picture" type="file" value="Загрузить"/>
                    </div>

                    <div class="form-group">
                        <div class="rloc">Дата</div>
                        <input name="date" type="datetime-local" rows="5"/>
                    </div>
                    <div class="form-group">
                        <div class="rloc">Выберите категорию</div>
                        <?php Auth::SelectList()?></br></br>
                    </div>

                    <div class="form-group">
                        <input type=submit style=width:40%; name="addEvent" value="Добавить событие" class="buttonS"/>
                        <input type=reset style=width:40%; value="Отмена" class="buttonS"/>
                    </div>

                </form>
                <a class="close" title="Закрыть" href="#close"></a>

            </div>


            <a href="#x" class="overlay" id="box1"></a>
            <div class="popup">
                <p>Редактировать данные пользователя</p>

                <form method=POST>
                    <div class="form-group">
                        <div class="rloc">Логин</div>
                        <input type=text name="login"/>
                    </div>

                    <div class="form-group">
                        <div class="rloc">Почта</div>
                        <input type="text" name="email"/><br>
                    </div>
					<div style=text-align:left;margin-left:220px;>
					<p>Тематики событий</p>
					<?php
						$qstring = 'SELECT * FROM `category`';
						$result = mysqli_query(Auth::$connection, $qstring) or die("Ошибочный запрос к БД" . mysqli_error());
						$row = mysqli_fetch_array($result);
						do {
						echo('<input type="checkbox" value="'.$row['id'].'" name="categories[]"/>'.$row['name'].'<br/>');
						} while ($row = mysqli_fetch_array($result));
					?>
					</div>
                    <div style=text-align:center;>
                        <input type=submit style=width:40%; value="Изменить" class="buttonS"/>
                        <input type=reset style=width:40%; value="Отмена" class="buttonS"/>
                        <a class="buttonS" href="changepassword.php" style=width:40%;>Сменить пароль</a>
                    </div>

                </form>

                <a class="close" title="Закрыть" href=""></a>
            </div>
            <div class="" role="alert" id="notif">
                
            </div>
        </div>

    </div>

    <div class="footer">
        <div style=position:absolute;bottom:0px;left:42%;><p>Авторы проекта: Ребенко Д., Ракова А.,Луценко О. </p></div>
    </div>

<?php else:{
echo '<script> location.replace("login.php");</script>';
}	?>

<?php endif; ?>


</body>
</html>