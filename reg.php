<?php

require 'config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">


<head>
	<script type="text/javascript" src="script/jquery-1.8.2.min.js"></script>
    <title>
        Registration
    </title>
    <link rel="stylesheet" href="style/bootstrap.css">
    <link rel="stylesheet" href="style/style.css">
</head>
<body>


<div class = "greeting">
			<h3 style=font-size:40px;>Для дальнейшей работы с сайтом зарегистрируйтесь.</h3>
		</div>
		<div class = "regform">
			<p style=padding-left:43%;>Регистрация</p>
			<form method = POST >
				<div class="form-group">
					<div class="rloc">Логин</div>
					<input type = text id = "login"/>
				</div>
				<div class="form-group">
					<div class="rloc">Пароль</div>
					<input type = password id = "password"/>
				</div>
				<div class="form-group">
					<div class="rloc">Повторите пароль</div>
					<input type = password id = "repeatpassword"/>
				</div>
				<div class="form-group">
					<div class="rloc">Почта</div>
					<input type="text" id="email"  /><br>
				</div>
				
				<div style=text-align:center;>
					<input type="button" id="submit" style=width:40%; value = "Зарегистрироваться" name="reg" class = "buttonS"/>
					<input type = reset style=width:40%; value = "Отмена" class = "buttonS"/>

				</div>
				<div class="alert alert-danger" role="alert" id ="info">Да, это не смешно</div>
				
			</form>
		</div>
		<div class="footer"><div style=position:absolute;bottom:0px;left:42%;><p>Авторы проекта: Ребенко Д., Ракова А.,Луценко О. </p></div></div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#submit").click(function() {
			var log = document.getElementById("login").value;
			var pass= document.getElementById("password").value;
			var repeatpass = document.getElementById("repeatpassword").value;
			var mail = document.getElementById("email").value;

			var information = document.getElementById("info");
			$.ajax({
					type: "POST", //Метод отправки
					url: "registration.php", //путь до php фаила отправителя
					data: {login: log, password: pass, repeatpassword: repeatpass, email: mail},
					success: function (data) {
						
						if (data.length == 0) {
							information.style.display = 'block';
							information.className = "alert alert-success";
							information.innerHTML = "Регистрация успешна";
						}
						else {
							information.className = "alert alert-danger";
							information.style.display = 'block';
							information.innerHTML = data;
						}

					}
				});

		});
	});
</script>
</body>
</html>