<?php
require 'config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
    <title>
        Смена пароля
    </title>
    <link rel="stylesheet" href="style/bootstrap.css">
    <link rel="stylesheet" href="style/style.css">
</head>
<body>



<?php
if($auth->authCheck($connection))
{

   $form= '<div class = "authForm">
			<p>Пожалуйста авторизируйтесь</p>
			<form method = POST >
                Старый пароль <br>
				<input type = text name = "oldPassword"/><br>
                Новый пароль <br>
				<input type = password name = "newPassword"/><br>
		        Повторите пароль <br>		
				<input type = password name = "repeatNewPassword"/><br>
				<input type = submit value = "Сменить пароль" name="changePassword" class = "buttonS" style=width:49%;/>
			</form>
		</div>';


    if(isset($_POST['changePassword']))
    {
        $result = $auth->ChangePassword($_SESSION['login'], $_POST['oldPassword'],$_POST['newPassword'], $_POST['repeatNewPassword']);
        if(is_bool($result))
        {
            echo $form;
            echo "<font color='green'> Пароль успешно изменён</font>";
        }
        else
        {
            echo $form;
            echo "<font color='green'> $result </font>";
        }


    }
    else
    {
        echo $form;
    }




}
else
{
    header("Location: login.php");

}


?>


</body>
</html>
