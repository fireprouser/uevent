<?php

require 'config.php';


if(isset($_POST['login'])  AND isset($_POST['password'])  AND isset($_POST['repeatpassword']) AND isset($_POST['email']) )
{

  Registation($_POST['login'], $_POST['password'], $_POST['repeatpassword'], $_POST['email']);

}



function Registation($login, $password, $repeatpassword, $email)
{
  $error = UserCheck($login, $password, $repeatpassword, $email);

  if (is_bool($error)) {

    // Во время регистрации мы применяем двойное хеширование для имитации безопасности
    mysqli_query(Auth::$connection, "INSERT INTO `users`(`login`, `password`, `email`) VALUES ('" . $login . "','" . md5(md5($password)) . "','" . $email . "');");

  } else {

    echo $error;
  }

}

// Функция проверки заполнения полей
function UserCheck($login, $password, $repeatpassword, $email)
{


  if (!preg_match("/^[a-z]+([-_]?[a-z0-9]+){0,2}$/", $login)) return "Логин может содержать только латинские буквы, цифры, подчеркивания и тире";
  if (!isset($password) or !isset($login) or !isset($repeatpassword)) return  "Все поля должны быть заполнены";
  if ($password != $repeatpassword) return "Пароли не совпадают.";
  if (strlen($password) < 4 or strlen($password) > 32) return "Пароль не должен быть короче 6 и длинее 32 символов";
  if (strlen($login) < 3 or strlen($login) > 32) return "Логин не должен быть короче 6 и длинее 32 символов";
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) return 'Не корректный email';

  //Проверяем есть ли пользователь с таким логином в базе данных
  $query = "SELECT * FROM users WHERE login='" . $login . "';";
  $result = mysqli_query(Auth::$connection, $query);

  if (mysqli_num_rows($result) > 0) {
    return 'Пользователь с таким именем уже существует';
  }
  //Проверяем есть ли пользователь с таким email в базе данных
  $query = "SELECT * FROM users WHERE email='" . $email . "';";
  $result = mysqli_query(Auth::$connection, $query);

  if (mysqli_num_rows($result) > 0) {
    return  'Пользователь с такой почтой уже существует';
  }
  return true;

}


