<?php

class Auth
{
    /*
     * Функция регистрации принимает на вход все данные с поля регистрации с помощью функции userCheck проверяет правильность
     * введённых данных, если данные верны то регистрируемся, если нет возвращаем массив ошибок
    */
    static $connection;

    function changeConnection($connect)
    {
        Auth::$connection = $connect;
    }

    function Registation($login, $password, $repeatpassword, $email)
    {
        $error = $this->UserCheck($login, $password, $repeatpassword, $email);

        if (count($error) == 0) {
            // Во время регистрации мы применяем двойное хеширование для имитации безопасности
            mysqli_query(Auth::$connection, "INSERT INTO `users`(`login`, `password`, `email`) VALUES ('" . $login . "','" . md5(md5($password)) . "','" . $email . "');");
            return true;
        } else {
            return $error;
        }

    }

    // Функция проверки заполнения полей
    function UserCheck($login, $password, $repeatpassword, $email)
    {

        $error = array();
        if (!preg_match("/^[a-z]+([-_]?[a-z0-9]+){0,2}$/", $login)) $error[] = "Логин может содержать только латинские буквы, цифры, подчеркивания и тире";
        if (!isset($password) or !isset($login) or !isset($repeatpassword)) $error[] = "Все поля должны быть заполнены";
        if ($password != $repeatpassword) $error[] = "Пароли не совпадают.";
        if (strlen($password) < 4 or strlen($password) > 32) $error[] = "Пароль не должен быть короче 6 и длинее 32 символов";
        if (strlen($login) < 3 or strlen($login) > 32) $error[] = "Логин не должен быть короче 6 и длинее 32 символов";
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) $error[] = 'Не корректный email';
        if (count($error) > 0) {
            return $error;
        }

        //Проверяем есть ли пользователь с таким логином в базе данных
        $query = "SELECT * FROM users WHERE login='" . $login . "';";
        $result = mysqli_query(Auth::$connection, $query);

        if (mysqli_num_rows($result) > 0) {
            $error[] = 'Пользователь с таким именем уже существует';
        }
        //Проверяем есть ли пользователь с таким email в базе данных
        $query = "SELECT * FROM users WHERE email='" . $email . "';";
        $result = mysqli_query(Auth::$connection, $query);

        if (mysqli_num_rows($result) > 0) {
            $error[] = 'Пользователь с такой почтой уже существует';
        }

        return $error;
    }

    /*
     * Функция проверки сессии у пользователя, проверяет куки текущего пользователя, если пользователь с данным логином
     * и паролем(который захеширован) есть в БД, то возвращаем TRUE, если совпадений не найдено чистим ему куки и возвращаем FALSE
    */
    function AuthCheck()
    {
        if (isset($_SESSION['login']) AND isset($_SESSION['hash'])) return true;

        if (isset($_COOKIE['login']) and isset($_COOKIE['hash'])) {

            $query = "SELECT * FROM users WHERE login='" . $_COOKIE['login'] . "' AND password ='" . $_COOKIE['hash'] . "';";
            $result = mysqli_query(Auth::$connection, $query);
            $userdata = mysqli_fetch_assoc($result);

            // Сравниваем данные с базы с данными в куках, если что-то не совпадает чистим куки
            if ($userdata['login'] != $_COOKIE['login'] or $userdata['password'] != $_COOKIE['hash']) {
                setcookie("login", "", time() - 3600 * 24 * 30 * 12, "/");
                setcookie("hash", "", time() - 3600 * 24 * 30 * 12, "/");
                return false;
            } else {
                $_SESSION['login'] = $userdata['login'];
                $_SESSION['hash'] = $userdata['password'];
                return true;
            }

        } else {
            return false;
        }

    }

    /*
     * Функция авторизации, проверяет есть ли пользователь в БД, если да, то даёт ему куки с его логином и паролем,
     * которые в дальнейшем используются для авторизации
     */
    function Login($login, $password)
    {
        $hashedPassword = md5(md5($password));

        $query = "SELECT * FROM users WHERE login='" . $login . "' AND password ='" . $hashedPassword . "';";
        $result = mysqli_num_rows(mysqli_query(Auth::$connection, $query));

        if ($result == 1) {
            $_SESSION['login'] = $login;
            $_SESSION['hash'] = $hashedPassword;

            setcookie('login', $login, time() + 60 * 60 * 24 * 30);
            setcookie('hash', $hashedPassword, time() + 60 * 60 * 24 * 30);
            return true;
        } else {
            return false;
        }

    }
    function AddEvent($file,$title, $decription, $date, $author, $category)
    {
        $uploaddir = 'image/';
        $uploadfile = $uploaddir .$this->generateRandomString(6). basename($_FILES[$file]['name']);

        $result = $this->checkEvent($file,$title,$decription,$date,$author);
        if(is_bool($result))
        {
            if(move_uploaded_file($_FILES[$file]['tmp_name'], $uploadfile))
            {

                $query = "INSERT INTO `events`(`id`, `title`, `content`,`date`, `image`, `author`,`idcategory`) VALUES (NULL ,'".$_POST[$title]."','".$_POST[$decription]."','".$_POST[$date]."','".$uploadfile."','".$_SESSION[$author]."',".$_POST[$category].");";

                mysqli_query(Auth::$connection, $query) or die("Ошибка запроса");


            }
            else
            {
                return $result;
            }

        }

        else
        {
            return $result;
        }

    }
    function generateRandomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    function checkEvent($file,$title, $decription, $date, $author )
    {

        if(!isset($_POST[$title]) or !isset($_POST[$decription]) or !isset($_POST[$date]) ) return "Должны быть заполнены все поля";
        if(strlen($_POST[$title]) < 6 or strlen($_POST[$title]) > 40) return "Заголовок должен быть длиной от 6 до 40 символов";
        if(strlen($_POST[$decription]) < 40 or strlen($_POST[$decription]) > 400) return "Описание должно содержать от 40 до 400 символов";

        if (isset($_FILES[$file]['name']))
        {
            //проверка расширения файла
            $file_name = $_FILES[$file]['name'];
            $filemas = explode(".", $_FILES[$file]['name']);
            $filetype = $filemas[count($filemas) - 1];
            if ($filetype == "jpg" or
                $filetype == "jpeg" or
                $filetype == "gif" or
                $filetype == "bmp" or
                $filetype == "png")
            {

                if($_FILES[$file]['size'] != 0
                    AND $_FILES[$file]['size']<=819200)
                {
                    //проверяем функцией is_uploaded_file
                    if(!is_uploaded_file($_FILES[$file]['tmp_name']))
                    {
                        return "Ошибка файла.";

                    }
                }
                else{
                    return "Размер файла превышает 800 КБ.";
                }
            }
            else{
                return "Файл не является изображением";
            }
        }
        else
        {
            return "Изображение не загружено, рекомендуем загрузить изображением";
        }
        return true;


        }
    function ChangePassword($login, $oldPassword, $newPassword, $reapeatNewPassword)
    {

        if ($newPassword == $reapeatNewPassword) {
            $query = "SELECT * FROM users WHERE login='" . $login . "' AND password ='" . md5(md5($oldPassword)) . "';";
            $result = mysqli_num_rows(mysqli_query(Auth::$connection, $query));
            if ($result == 1) {
                $query = "UPDATE users SET password ='" . md5(md5($newPassword)) . "' WHERE  login='" . $login . "' ;";
                mysqli_query(Auth::$connection, $query);
                return true;
            }
            $error = "Старый пароль введён неверно.";
            return $error;


        } else {
            $error = "Пароли не совпадают.";
            return $error;
        }


    }
    function Logout()
    {
        session_destroy();
        if(isset($_COOKIE['login']) AND isset($_COOKIE['hash']) )
        {
            setcookie('login', "", time() - 3600);
            setcookie('hash', "", time() - 3600);
            header("Location: login.php");
        }

    }
	
	static function GeneralOutput()
	{
		$result = mysqli_query(Auth::$connection, 'SELECT events.id, events.title, events.content, events.date,events.image, category.name FROM events,category WHERE events.idcategory=category.id ORDER BY date DESC') or die("Ошибочный запрос к БД" . mysqli_error());
            if ($result == NULL) echo 'Событий нет';
            else {
                $row = mysqli_fetch_array($result);
                do {
                    echo '<div class="event">';
                    echo '<h3>' . $row['title'] . '</h3>';
                    echo '<img class ="imgd" src="' . $row['image'] . '"/>';
                    echo '<div class="defin">' . $row['content'] . '<br/> ' . substr($row['date'], 0, 16) . ' <a class="category">'.$row['name'].'</a></div>';
                    echo '<a id="' . $row['id'] . '" class="subscribe" style=width:40%; name="reg">Подписаться</a></div>';

                } while ($row = mysqli_fetch_array($result));
            }
	}
	static function MyOutput($login)
	{

		$qstring = 'SELECT events.id, events.title, events.content, events.date,events.image, category.name FROM `events`,category WHERE events.idcategory=category.id and `author`="' . $login . '" ORDER BY date DESC;';
		$result = mysqli_query(Auth::$connection, $qstring) or die("Ошибочный запрос к БД" . mysqli_error());
            if ($result == NULL) echo 'Событий нет';
            else {
                $row = mysqli_fetch_array($result);
                do {
                    echo '<div class="event">';
                    echo '<h3>' . $row['title'] . '</h3>';
                    echo '<img class ="imgd" src="' . $row['image'] . '"/>';
                    echo '<div class="defin">' . $row['content'] . '<br/> ' . substr($row['date'], 0, 16) . '<a class="category">'.$row['name'].'</a></div>';
					echo '<a name="reg" >Изменить</a>';
					echo '<a name="reg" style=margin-left:20px;>Удалить</a></div>';
                } while ($row = mysqli_fetch_array($result));
            }
	}
	static function SubOutput($login)
	{

		$qstring = 'SELECT events.id, events.title, events.content, events.date,events.image, category.name FROM `events`,`subscribe`,category WHERE events.idcategory=category.id and `events`.`id`=`subscribe`.`eventid` and `login`="' . $login . '" ORDER BY date DESC;';

		$result = mysqli_query(Auth::$connection, $qstring) or die("Ошибочный запрос к БД" . mysqli_error());
            if ($result == NULL) echo 'Событий нет';
            else {
                $row = mysqli_fetch_array($result);
                do {
                    echo '<div class="event">';
                    echo '<h3>' . $row['title'] . '</h3>';
                    echo '<img class ="imgd" src="' . $row['image'] . '"/>';
                    echo '<div class="defin">' . $row['content'] . '<br/> ' . substr($row['date'], 0, 16) . '<a class="category">'.$row['name'].'</a></div></div>';
                } while ($row = mysqli_fetch_array($result));
            }
	}
    static function SelectList()
    {
        $query = 'SELECT * FROM `category`;';
        $result = mysqli_query(Auth::$connection, $query);
        if($result!=NULL)
        {
            echo '<select size="1" name="category">';
            $category = mysqli_fetch_array($result);
            do
            {

                echo '<option value="'.$category['id'].'">'.$category['name'].'</option>';


            } while($category = mysqli_fetch_array($result));
            echo '</select>';
        }
    }


}


?>